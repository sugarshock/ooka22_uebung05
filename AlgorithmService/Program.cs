using AlgorithmService;

var builder = WebApplication.CreateBuilder(args);

// Circuit Breaker
builder.Services.AddHttpClient<JobService>(client => { client.BaseAddress = new Uri(builder.Configuration.GetConnectionString("AnalyzerUrl")); })
    .AddPolicyHandler(StartupUtils.GetCircuitBreakerPolicy())
    .AddPolicyHandler(StartupUtils.GetRetryPolicy());

// Add services to the container.
builder.Services.AddSingleton<JobService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();



