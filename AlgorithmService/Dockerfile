#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["AlgorithmService/AlgorithmService.csproj", "AlgorithmService/"]
COPY ["EngineConfigAnalyzer/EngineConfigAnalyzer.API.csproj", "EngineConfigAnalyzer/"]
COPY ["EngineConfigAnalyzer.Domain/EngineConfigAnalyzer.Domain.csproj", "EngineConfigAnalyzer.Domain/"]
RUN dotnet restore "AlgorithmService/AlgorithmService.csproj"
COPY . .
WORKDIR "/src/AlgorithmService"
RUN dotnet build "AlgorithmService.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "AlgorithmService.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "AlgorithmService.dll"]