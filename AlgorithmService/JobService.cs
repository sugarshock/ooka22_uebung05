﻿using EngineConfigAnalyzer.API.DTOs;
using EngineConfigAnalyzer.Domain.Entities;

namespace AlgorithmService
{
    public class JobService
    {

        private readonly IHttpClientFactory _httpClientFactory;
        public AlgorithmJobAssignement CurrentJob { get; private set; }
        private Queue<AlgorithmJobAssignement> jobQueue = new Queue<AlgorithmJobAssignement>();

        public bool IsBusy { get => (CurrentJob != null); }

        public JobService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public void QueueNewJob(AlgorithmJobAssignement configJob)
        {
            jobQueue.Enqueue(configJob);

            if (!IsBusy)
            {
                new Thread(() => AnalyzeNext()).Start();
            }
        }


        public async Task AnalyzeNext()
        {
            CurrentJob = jobQueue.Dequeue();
            if (CurrentJob != null)
            {
                return;
            }
            System.Threading.Thread.Sleep(10000);
            var result = AnalyzeResult();
            var updated = await UpdateStatus(CurrentJob, result);

            if (updated)
            {
                new Thread(() => AnalyzeNext()).Start();
            }
            else
            {
                Console.WriteLine("Unable to send analyzation result to ECA. Stopped working!");
            }
        }

        public JOB_STATUS AnalyzeResult()
        {
            return EngineConfigAnalyzer.Domain.Entities.JOB_STATUS.READY;
        }


        private async Task<bool> UpdateStatus(AlgorithmJobAssignement job, JOB_STATUS newStatus)
        {
            var client = _httpClientFactory.CreateClient();
            var endpoint = String.Format("{0}/equipJobs/{1}", job.AnalyzerJobId, job.Id);

            var response = await client.PutAsJsonAsync<JOB_STATUS>(endpoint, newStatus);

            return response.IsSuccessStatusCode;
        }
    }
}
