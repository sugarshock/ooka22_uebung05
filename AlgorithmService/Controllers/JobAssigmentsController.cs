﻿using EngineConfigAnalyzer.API.DTOs;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AlgorithmService.Controllers
{
    [Route("api/v1/jobassigments")]
    [ApiController]
    public class JobAssigmentsController : ControllerBase
    {
        private JobService _jobService;

        public JobAssigmentsController(JobService jobService)
        {
            _jobService = jobService;
        }

        // GET api/<JobsController>/5
        [HttpGet()]
        public async Task<ActionResult<AlgorithmJobAssignement>> GetCurrentJob()
        {
            return _jobService.IsBusy ? Ok(_jobService.CurrentJob) : NotFound();
        }

        // POST api/<JobsController>
        [HttpPost]
        public async Task<ActionResult> PostJob(AlgorithmJobAssignement equipJobAssignement)
        {
            _jobService.QueueNewJob(equipJobAssignement);
            return Ok();
        }
    }
}
