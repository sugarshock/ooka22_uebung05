﻿using System.ComponentModel.DataAnnotations;

namespace EngineConfigAnalyzer.Domain.Entities
{
    public class AnalyzerJob
    {
        [Key]
        public Guid Id { get; private set; }
        public String OrderId { get; private set; }

        public OptionalEquipment OptionalEquipment { get; private set; }

        public Dictionary<Guid, AlgorithmJob> AlgorithmJobs { get; private set; }

        public AnalyzerJob() { }

        public AnalyzerJob(Guid id, string orderId, OptionalEquipment optionalEquipment)
        {
            Id = id;
            OrderId = orderId;
            OptionalEquipment = optionalEquipment;
            AlgorithmJobs = new Dictionary<Guid, AlgorithmJob>();
        }

        public Guid AddEquipJob(string algoName)
        {
            var id = Guid.NewGuid();
            var job = new AlgorithmJob(algoName);
            AlgorithmJobs.Add(id, job);
            return id;
        }

        public void UpdateAlgorithmJobStatus(Guid jobId, JOB_STATUS status)
        {
            if (!AlgorithmJobs.ContainsKey(jobId))
            {
                throw new KeyNotFoundException("AlgorithmJob does not belong to this AnalyzerJob");
            }
            var job = AlgorithmJobs[jobId];
            job.JobStatus = status;
        }
    }
}
