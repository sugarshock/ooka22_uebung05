﻿using Microsoft.EntityFrameworkCore;

namespace EngineConfigAnalyzer.Domain.Entities
{
    public enum JOB_STATUS { RUNNING, FAILED, READY, NOT_STARTED }

    [Owned]
    public class AlgorithmJob
    {
        public string JobName { get; set; }
        public JOB_STATUS JobStatus { get; set; }

        public AlgorithmJob(string jobName)
        {
            JobName = jobName;
            JobStatus = JOB_STATUS.NOT_STARTED;
        }
    }
}
