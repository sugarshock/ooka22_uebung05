﻿using Microsoft.EntityFrameworkCore;

namespace EngineConfigAnalyzer.Domain.Entities
{
    [Owned]
    public record OptionalEquipment
    {
        public String? StartingSystem { get; set; }
        public String? AuxiliaryPTO { get; set; }
        public String? OilSystem { get; set; }
        public String? FuelSystem { get; set; }
        public String? CoolingSystem { get; set; }
        public String? ExhaustSystem { get; set; }
        public String? MountingSystem { get; set; }
        public String? EngineManagementSystem { get; set; }
        public String? MonitoringControllSystem { get; set; }
        public String? PowerTransmission { get; set; }
        public String? GearboxOptions { get; set; }

    }
}
