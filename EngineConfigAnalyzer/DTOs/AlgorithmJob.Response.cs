﻿using EngineConfigAnalyzer.Domain.Entities;

namespace EngineConfigAnalyzer.API.DTOs
{
    public record AlgorithmJobResponse
    {
        public string JobName { get; set; }
        public JOB_STATUS JobStatus { get; set; }
    }
}
