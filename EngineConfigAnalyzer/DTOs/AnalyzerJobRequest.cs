﻿using EngineConfigAnalyzer.Domain.Entities;

namespace EngineConfigAnalyzer.API.DTOs
{
    public record AnalyzerJobRequest
    {
        public String OrderId { get; set; }
        public OptionalEquipment OptionalEquipment { get; set; }
    }
}
