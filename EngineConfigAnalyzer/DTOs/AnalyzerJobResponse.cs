﻿using EngineConfigAnalyzer.Domain.Entities;

namespace EngineConfigAnalyzer.API.DTOs
{
    public record AnalyzerJobResponse
    {
        public Guid Id { get; set; }
        public String OrderId { get; set; }
        public OptionalEquipment OptionalEquipment { get; set; }

        public List<AlgorithmJobResponse> AlgorithmJobs { get; set; }

    }
}
