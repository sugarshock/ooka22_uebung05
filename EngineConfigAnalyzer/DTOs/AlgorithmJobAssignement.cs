﻿using EngineConfigAnalyzer.Domain.Entities;

namespace EngineConfigAnalyzer.API.DTOs
{
    public record AlgorithmJobAssignement
    {
        public Guid Id { get; set; }
        public Guid AnalyzerJobId { get; set; }
        public string JobName { get; set; }
        public JOB_STATUS JobStatus { get; set; }

        public OptionalEquipment OptionalEquipment { get; set; }
    }
}
