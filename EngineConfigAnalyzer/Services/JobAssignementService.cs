﻿using EngineConfigAnalyzer.API.DTOs;
using EngineConfigAnalyzer.API.Infrastructure;
using EngineConfigAnalyzer.Domain.Entities;

namespace EngineConfigAnalyzer.API.Services
{
    public class JobAssignementService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        private readonly AnalyzerContext _analyzerContext;

        private readonly string _endpoint = "/api/v1/jobassigments";

        private readonly string _liquidsAlgoUrl;
        private readonly string _mechanicsAlgoUrl;
        private readonly string _electronicsAlgoUrl;

        public JobAssignementService(IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            _httpClientFactory = httpClientFactory;
            _configuration = configuration;

            _liquidsAlgoUrl = _configuration["Algorithms:Liquids"];
            _mechanicsAlgoUrl = _configuration["Algorithms:Mechanics"];
            _electronicsAlgoUrl = _configuration["Algorithms:Electronics"];
        }

        public async Task Schedule(AnalyzerJob configJob)
        {
            var liquidsId = configJob.AddEquipJob("Liquids");
            var mechanicsId = configJob.AddEquipJob("Mechanics");
            var electronicsId = configJob.AddEquipJob("Electronics");

            Assign(_liquidsAlgoUrl, configJob, liquidsId).Wait();
            Assign(_mechanicsAlgoUrl, configJob, mechanicsId).Wait();
            Assign(_electronicsAlgoUrl, configJob, electronicsId).Wait();
        }

        public async Task Assign(string algoUrl, AnalyzerJob configJob, Guid equipJobId)
        {
            var url = "http://" + algoUrl + _endpoint;
            var equipJob = configJob.AlgorithmJobs[equipJobId];

            if (equipJob == null)
            {
                Console.WriteLine("Something went wrong. Tried to assign an EquipJob that doesnt exist!");
            }

            if (equipJob.JobStatus != JOB_STATUS.NOT_STARTED)
            {
                Console.WriteLine("Tried to assign an EquipJob that was already analyzed!");
            }

            var assignement = new AlgorithmJobAssignement()
            {
                Id = equipJobId,
                AnalyzerJobId = configJob.Id,
                OptionalEquipment = configJob.OptionalEquipment,
                JobName = equipJob.JobName,
                JobStatus = equipJob.JobStatus
            };

            var client = _httpClientFactory.CreateClient();

            HttpResponseMessage response;
            try
            {
                response = await client.PostAsJsonAsync(url, assignement);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't assign job...");
            }
        }
    }
}
