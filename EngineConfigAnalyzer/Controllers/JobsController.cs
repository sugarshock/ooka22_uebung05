﻿using EngineConfigAnalyzer.API.DTOs;
using EngineConfigAnalyzer.API.Infrastructure;
using EngineConfigAnalyzer.API.Services;
using EngineConfigAnalyzer.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;


namespace EngineConfigAnalyzer.Controllers
{
    [Route("api/v1/jobs")]
    [ApiController]
    public class JobsController : ControllerBase
    {
        private readonly AnalyzerContext _analyzerContext;
        private readonly JobAssignementService _jobAssignementService;

        public JobsController(AnalyzerContext analyzerContext, JobAssignementService jobAssignementService)
        {
            _analyzerContext = analyzerContext;
            _jobAssignementService = jobAssignementService;
        }



        [HttpGet]
        public async Task<List<AnalyzerJobResponse>> GetAllAsync()
        {
            return await _analyzerContext.AnalyzerJobs.Select(job => AnalyzerJobToResponse(job)).ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AnalyzerJobResponse>> GetByIdAsync(Guid id)
        {
            var job = await _analyzerContext.AnalyzerJobs.FindAsync(id);
            if (job == null)
            {
                return NotFound();
            }
            var response = AnalyzerJobToResponse(job);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> PostJobAsync(AnalyzerJobRequest analyzerJobRequest)
        {
            var configJob = new AnalyzerJob(Guid.NewGuid(), analyzerJobRequest.OrderId, analyzerJobRequest.OptionalEquipment);
            await _analyzerContext.AnalyzerJobs.AddAsync(configJob);

            await _jobAssignementService.Schedule(configJob);
            await _analyzerContext.SaveChangesAsync();
            return Ok(AnalyzerJobToResponse(configJob));
        }

        [HttpPut("{id}/equipJobs/{equipdJobId}")]
        public async Task<IActionResult> UpdateAlgorithmJobStatus(Guid id, Guid algorithmJobId, JOB_STATUS status)
        {
            var job = await _analyzerContext.AnalyzerJobs.FindAsync(id);
            if (job == null)
            {
                return NotFound();
            }

            try
            {
                job.UpdateAlgorithmJobStatus(algorithmJobId, status);
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            await _analyzerContext.SaveChangesAsync();
            return Ok();

        }

        /*// DELETE api/<ConfigJobController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/

        private static AnalyzerJobResponse AnalyzerJobToResponse(AnalyzerJob job)
        {
            return new AnalyzerJobResponse
            {
                Id = job.Id,
                OrderId = job.OrderId,
                OptionalEquipment = job.OptionalEquipment,
                AlgorithmJobs = job.AlgorithmJobs.Select(equip => AlgorithmJobToResponse(equip.Value)).ToList<AlgorithmJobResponse>()
            };
        }

        private static AlgorithmJobResponse AlgorithmJobToResponse(AlgorithmJob job)
        {
            return new AlgorithmJobResponse
            {
                JobStatus = job.JobStatus,
                JobName = job.JobName,
            };
        }
    }
}
