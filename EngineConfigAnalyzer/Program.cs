using EngineConfigAnalyzer;
using EngineConfigAnalyzer.API.Infrastructure;
using EngineConfigAnalyzer.API.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Circuit Breaker
builder.Services.AddHttpClient<JobAssignementService>(client => { client.BaseAddress = new Uri(builder.Configuration.GetConnectionString("AnalyzerUrl")); })
    .AddPolicyHandler(StartupUtils.GetCircuitBreakerPolicy())
    .AddPolicyHandler(StartupUtils.GetRetryPolicy());

// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<AnalyzerContext>(options => options.UseNpgsql(connectionString));
builder.Services.AddSingleton<JobAssignementService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI((c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
    }));
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
