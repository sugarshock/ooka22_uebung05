﻿using EngineConfigAnalyzer.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace EngineConfigAnalyzer.API.Infrastructure
{
    public class AnalyzerContext : DbContext
    {
        public DbSet<AnalyzerJob> AnalyzerJobs { get; set; }



        public AnalyzerContext(DbContextOptions<AnalyzerContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AnalyzerJob>(b =>
            {

                b.Property(u => u.AlgorithmJobs)
                    .HasConversion(
                        d => JsonConvert.SerializeObject(d, Formatting.None),
                        s => JsonConvert.DeserializeObject<Dictionary<Guid, AlgorithmJob>>(s)
                    )
                    .HasMaxLength(4000)
                    .IsRequired();
            });
        }


    }
}
