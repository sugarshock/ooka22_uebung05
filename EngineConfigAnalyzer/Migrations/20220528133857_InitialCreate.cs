﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngineConfigAnalyzer.API.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ConfigJobs",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    OrderId = table.Column<string>(type: "text", nullable: false),
                    OptionalEquipment_StartingSystem = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_AuxiliaryPTO = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_OilSystem = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_FuelSystem = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_CoolingSystem = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_ExhaustSystem = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_MountingSystem = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_EngineManagementSystem = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_MonitoringControllSystem = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_PowerTransmission = table.Column<string>(type: "text", nullable: true),
                    OptionalEquipment_GearboxOptions = table.Column<string>(type: "text", nullable: true),
                    EquipJobs = table.Column<string>(type: "character varying(4000)", maxLength: 4000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfigJobs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConfigJobs");
        }
    }
}
